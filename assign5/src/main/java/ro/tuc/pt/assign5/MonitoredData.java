package ro.tuc.pt.assign5;

import java.time.LocalDateTime;

public class MonitoredData {

	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private String activity;

	public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity) {

		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;

	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public int getDay(){
        return this.getStartTime().getDayOfMonth();
    }
	
	public int getHour(){
        return this.getStartTime().getHour();
    }
	public int getMinute(){
        return this.getStartTime().getMinute();
    }
	
	
	public int toMins(LocalDateTime date) {
		String s=Integer.toString(date.getHour()) + ":" + Integer.toString(date.getMinute()) ;
	    String[] hourMin = s.split(":");
	    
	    int hour = Integer.parseInt(hourMin[0]);
	    int mins = Integer.parseInt(hourMin[1]);
	    int hoursInMins = hour * 60;
	    return hoursInMins + mins;
	}
	
	public String toString() {
		return "Start Time = " + startTime + ", End Time = " + endTime + ", Activity = " + activity  + "\n";
	}

}
