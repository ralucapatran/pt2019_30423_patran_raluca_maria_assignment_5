package ro.tuc.pt.assign5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App

{

	private static String fName = "config/Activities.txt";

	static List<MonitoredData> lista = new ArrayList<MonitoredData>();

	public static void main(String[] args) {

		
		fileStream(lista);
		
		
		countDays(lista);
		

		countActivities(lista);

		
		countActivitiesPerDay(lista);

	}

	private static void fileStream(List<MonitoredData> data) {
		try {

			System.out.println("Group");
			System.out.println("\n");

			Stream<String> lines = Files.lines(Paths.get(fName)).flatMap(line -> Arrays.stream(line.split("\t\t")));

			String[] text = lines.toArray(size -> new String[size]);

			for (int i = 0; i < text.length; i += 3) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");

				LocalDateTime start = LocalDateTime.parse(text[i], formatter);
				LocalDateTime end = LocalDateTime.parse(text[i + 1], formatter);
				MonitoredData m = new MonitoredData(start, end, text[i + 2].trim());
				
				//System.out.println(m.toMins(end)-m.toMins(start));
				
				data.add(m);
			}

			System.out.println(data.toString());

		} catch (IOException io) {

			io.printStackTrace();
		}
	}
	
	public static Map<Integer, Long> countDays(List<MonitoredData> data) {

		System.out.println("\n");
		System.out.println("The number of days is: ");

		Map<Integer, Long> allActivities = App.lista.stream()
				.collect(Collectors.groupingBy(MonitoredData::getDay, Collectors.counting()));

		System.out.println(allActivities.size());

		return allActivities;
	}

	public static Map<String, Long> countActivities(List<MonitoredData> data) {

		System.out.println("\n");
		System.out.println("The number for each activity is: ");

		Map<String, Long> allActivities = App.lista.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));

		System.out.println(allActivities);

		return allActivities;
	}

	public static Map<String, Map<Integer, Long>> countActivitiesPerDay(List<MonitoredData> data) {

		System.out.println("\n");
		System.out.println("The number for each activity/day is: ");

		Map<String, Map<Integer, Long>> allActivities = App.lista.stream().collect(Collectors.groupingBy(
				MonitoredData::getActivity, Collectors.groupingBy(MonitoredData::getDay, Collectors.counting())));

		System.out.println(allActivities + "\n");

		return allActivities;
	}

}
